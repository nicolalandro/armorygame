// Auto-generated
let project = new Project('graduate_hat_1_0_3');

project.addSources('Sources');
project.addLibrary("/home/utente/apps/ArmorySDK2307/armsdk/armory");
project.addLibrary("/home/utente/apps/ArmorySDK2307/armsdk/iron");
project.addParameter('armory.trait.internal.UniformsManager');
project.addParameter("--macro keep('armory.trait.internal.UniformsManager')");
project.addParameter('arm.node.LogicTree');
project.addParameter("--macro keep('arm.node.LogicTree')");
project.addShaders("build_graduate_hat/compiled/Shaders/*.glsl", { noembed: false});
project.addAssets("build_graduate_hat/compiled/Assets/**", { notinlist: true });
project.addAssets("build_graduate_hat/compiled/Shaders/*.arm", { notinlist: true });
project.addAssets("/home/utente/apps/ArmorySDK2307/armsdk/armory/Assets/brdf.png", { notinlist: true });
project.addAssets("/home/utente/apps/ArmorySDK2307/armsdk/armory/Assets/smaa_area.png", { notinlist: true });
project.addAssets("/home/utente/apps/ArmorySDK2307/armsdk/armory/Assets/smaa_search.png", { notinlist: true });
project.addParameter('--debug');
project.addLibrary("/home/utente/apps/ArmorySDK2307/armsdk/lib/zui");
project.addAssets("/home/utente/apps/ArmorySDK2307/armsdk/armory/Assets/font_default.ttf", { notinlist: false });
project.addDefine('arm_deferred');
project.addDefine('arm_csm');
project.addDefine('rp_hdr');
project.addDefine('rp_renderer=Deferred');
project.addDefine('rp_shadowmap');
project.addDefine('rp_shadowmap_cascade=1024');
project.addDefine('rp_shadowmap_cube=512');
project.addDefine('rp_background=World');
project.addDefine('rp_render_to_texture');
project.addDefine('rp_compositornodes');
project.addDefine('rp_antialiasing=SMAA');
project.addDefine('rp_supersampling=1');
project.addDefine('rp_ssgi=SSAO');
project.addDefine('arm_assert_level=Warning');
project.addDefine('arm_soundcompress');
project.addDefine('arm_audio');
project.addDefine('arm_ui');
project.addDefine('arm_skin');
project.addDefine('arm_morph_target');
project.addDefine('arm_particles');
project.addDefine('arm_resizable');
project.addDefine('armory');


resolve(project);

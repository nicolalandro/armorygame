package arm.node;

@:access(armory.logicnode.LogicNode)@:keep class LogicTree extends armory.logicnode.LogicTree {

	var functionNodes:Map<String, armory.logicnode.FunctionNode>;

	var functionOutputNodes:Map<String, armory.logicnode.FunctionOutputNode>;

	public function new() {
		super();
		this.functionNodes = new Map();
		this.functionOutputNodes = new Map();
		notifyOnAdd(add);
	}

	override public function add() {
		var _RotateObject_001 = new armory.logicnode.RotateObjectNode(this);
		_RotateObject_001.property0 = "Local";
		_RotateObject_001.preallocInputs(3);
		_RotateObject_001.preallocOutputs(1);
		var _Branch = new armory.logicnode.BranchNode(this);
		_Branch.preallocInputs(2);
		_Branch.preallocOutputs(2);
		var _OnTimer_001 = new armory.logicnode.OnTimerNode(this);
		_OnTimer_001.preallocInputs(2);
		_OnTimer_001.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 0.10000000149011612), _OnTimer_001, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.BooleanNode(this, true), _OnTimer_001, 0, 1);
		armory.logicnode.LogicNode.addLink(_OnTimer_001, _Branch, 0, 0);
		var _Boolean = new armory.logicnode.BooleanNode(this);
		_Boolean.preallocInputs(1);
		_Boolean.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.BooleanNode(this, false), _Boolean, 0, 0);
		armory.logicnode.LogicNode.addLink(_Boolean, _Branch, 0, 1);
		armory.logicnode.LogicNode.addLink(_Branch, _RotateObject_001, 1, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _RotateObject_001, 0, 1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.RotationNode(this, -0.014997749589383602,0.0,0.0,0.9998875260353088), _RotateObject_001, 0, 2);
		armory.logicnode.LogicNode.addLink(_RotateObject_001, new armory.logicnode.NullNode(this), 0, 0);
		var _RotateObject = new armory.logicnode.RotateObjectNode(this);
		_RotateObject.property0 = "Local";
		_RotateObject.preallocInputs(3);
		_RotateObject.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(_Branch, _RotateObject, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _RotateObject, 0, 1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.RotationNode(this, 0.014997749589383602,0.0,0.0,0.9998875260353088), _RotateObject, 0, 2);
		armory.logicnode.LogicNode.addLink(_RotateObject, new armory.logicnode.NullNode(this), 0, 0);
		var _SetVariable = new armory.logicnode.SetVariableNode(this);
		_SetVariable.preallocInputs(3);
		_SetVariable.preallocOutputs(1);
		var _OnTimer = new armory.logicnode.OnTimerNode(this);
		_OnTimer.preallocInputs(2);
		_OnTimer.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 2.0), _OnTimer, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.BooleanNode(this, true), _OnTimer, 0, 1);
		armory.logicnode.LogicNode.addLink(_OnTimer, _SetVariable, 0, 0);
		armory.logicnode.LogicNode.addLink(_Boolean, _SetVariable, 0, 1);
		var _InvertBoolean = new armory.logicnode.NotNode(this);
		_InvertBoolean.preallocInputs(1);
		_InvertBoolean.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(_Boolean, _InvertBoolean, 0, 0);
		armory.logicnode.LogicNode.addLink(_InvertBoolean, _SetVariable, 0, 2);
		armory.logicnode.LogicNode.addLink(_SetVariable, new armory.logicnode.NullNode(this), 0, 0);
	}
}
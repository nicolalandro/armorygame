[![gitlab pages deployed page link](https://img.shields.io/badge/Gitlab-Pages-orange?logo=gitlab)](https://nicolalandro.gitlab.io/armorygame/)

# Armory Blender game
This repo contain an armory game in blender tryed to deploy on gitlab pages.

# References
* [Blender](https://www.blender.org/): For drawing 3D
* [Armory engine](https://armory3d.org/): a game engine for blender
* [Armory template](https://github.com/armory3d/armory_templates): some teplate of games
* [Armory Demo online](https://armory3d.github.io/armory_examples_browser/): online examles of templates
* [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/): a way to deploy free static websites like our game
* [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/): automation pipeline to automate the deploy of the site

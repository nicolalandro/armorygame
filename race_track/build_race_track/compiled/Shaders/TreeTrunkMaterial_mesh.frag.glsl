#version 450
#include "compiled.inc"
#include "std/gbuffer.glsl"
in vec3 wnormal;
out vec4 fragColor[GBUF_SIZE];
uniform float objectInfoRandom;
const vec3 COLORRAMP_COLS[3] = vec3[](vec3(0.019999999552965164, 0.05000000074505806, 0.012000000104308128), vec3(0.019999999552965164, 0.05000000074505806, 0.012000000104308128), vec3(0.019999999552965164, 0.05000000074505806, 0.012000000104308128));
const float COLORRAMP_FACS[3] = float[](0.0, 1.0, 1.0);
void main() {
	vec3 n = normalize(wnormal);
	vec3 basecol;
	float roughness;
	float metallic;
	float occlusion;
	float specular;
	vec3 emissionCol;
	float ObjectInfo_Random_res = objectInfoRandom;
	float ColorRamp_fac = ObjectInfo_Random_res;
	int ColorRamp_i = 0 + ((ColorRamp_fac > 1.0) ? 1 : 0);
	vec3 ColorRamp_Color_res = mix(COLORRAMP_COLS[ColorRamp_i], COLORRAMP_COLS[ColorRamp_i + 1], max((ColorRamp_fac - COLORRAMP_FACS[ColorRamp_i]) * (1.0 / (COLORRAMP_FACS[ColorRamp_i + 1] - COLORRAMP_FACS[ColorRamp_i])), 0.0));
	basecol = ColorRamp_Color_res;
	roughness = 0.0;
	metallic = 0.0;
	occlusion = 1.0;
	specular = 0.0;
	emissionCol = vec3(0.0);
	n /= (abs(n.x) + abs(n.y) + abs(n.z));
	n.xy = n.z >= 0.0 ? n.xy : octahedronWrap(n.xy);
	const uint matid = 0;
	fragColor[GBUF_IDX_0] = vec4(n.xy, roughness, packFloatInt16(metallic, matid));
	fragColor[GBUF_IDX_1] = vec4(basecol, packFloat2(occlusion, specular));
	#ifdef _EmissionShaded
	fragColor[GBUF_IDX_EMISSION] = vec4(emissionCol, 0.0);
	#endif
}

#version 330

uniform samplerCubeShadow shadowMapPoint[1];
uniform vec2 lightProj;
uniform sampler2D gbuffer0;
uniform sampler2D gbuffer1;
uniform sampler2D gbufferD;
uniform vec3 eye;
uniform vec3 eyeLook;
uniform vec2 cameraProj;
uniform sampler2D senvmapBrdf;
uniform vec4 shirr[7];
uniform int envmapNumMipmaps;
uniform sampler2D senvmapRadiance;
uniform float envmapStrength;
uniform sampler2D ssaotex;
uniform vec3 pointPos;
uniform vec3 pointCol;
uniform float pointBias;
uniform vec4 casData[20];

in vec2 texCoord;
in vec3 viewRay;
out vec4 fragColor;

vec2 octahedronWrap(vec2 v)
{
    return (vec2(1.0) - abs(v.yx)) * vec2((v.x >= 0.0) ? 1.0 : (-1.0), (v.y >= 0.0) ? 1.0 : (-1.0));
}

void unpackFloatInt16(float val, out float f, out uint i)
{
    uint bitsValue = uint(val);
    i = bitsValue >> 12u;
    f = float(bitsValue & 4294905855u) / 4095.0;
}

vec2 unpackFloat2(float f)
{
    return vec2(floor(f) / 255.0, fract(f));
}

vec3 surfaceAlbedo(vec3 baseColor, float metalness)
{
    return mix(baseColor, vec3(0.0), vec3(metalness));
}

vec3 surfaceF0(vec3 baseColor, float metalness)
{
    return mix(vec3(0.039999999105930328369140625), baseColor, vec3(metalness));
}

vec3 getPos(vec3 eye_1, vec3 eyeLook_1, vec3 viewRay_1, float depth, vec2 cameraProj_1)
{
    float linearDepth = cameraProj_1.y / (((depth * 0.5) + 0.5) - cameraProj_1.x);
    float viewZDist = dot(eyeLook_1, viewRay_1);
    vec3 wposition = eye_1 + (viewRay_1 * (linearDepth / viewZDist));
    return wposition;
}

vec3 shIrradiance(vec3 nor, vec4 shirr_1[7])
{
    vec3 cl00 = vec3(shirr_1[0].x, shirr_1[0].y, shirr_1[0].z);
    vec3 cl1m1 = vec3(shirr_1[0].w, shirr_1[1].x, shirr_1[1].y);
    vec3 cl10 = vec3(shirr_1[1].z, shirr_1[1].w, shirr_1[2].x);
    vec3 cl11 = vec3(shirr_1[2].y, shirr_1[2].z, shirr_1[2].w);
    vec3 cl2m2 = vec3(shirr_1[3].x, shirr_1[3].y, shirr_1[3].z);
    vec3 cl2m1 = vec3(shirr_1[3].w, shirr_1[4].x, shirr_1[4].y);
    vec3 cl20 = vec3(shirr_1[4].z, shirr_1[4].w, shirr_1[5].x);
    vec3 cl21 = vec3(shirr_1[5].y, shirr_1[5].z, shirr_1[5].w);
    vec3 cl22 = vec3(shirr_1[6].x, shirr_1[6].y, shirr_1[6].z);
    return ((((((((((cl22 * 0.429042994976043701171875) * ((nor.y * nor.y) - ((-nor.z) * (-nor.z)))) + (((cl20 * 0.743125021457672119140625) * nor.x) * nor.x)) + (cl00 * 0.88622701168060302734375)) - (cl20 * 0.2477079927921295166015625)) + (((cl2m2 * 0.85808598995208740234375) * nor.y) * (-nor.z))) + (((cl21 * 0.85808598995208740234375) * nor.y) * nor.x)) + (((cl2m1 * 0.85808598995208740234375) * (-nor.z)) * nor.x)) + ((cl11 * 1.02332794666290283203125) * nor.y)) + ((cl1m1 * 1.02332794666290283203125) * (-nor.z))) + ((cl10 * 1.02332794666290283203125) * nor.x);
}

float getMipFromRoughness(float roughness, float numMipmaps)
{
    return roughness * numMipmaps;
}

vec2 envMapEquirect(vec3 normal)
{
    float phi = acos(normal.z);
    float theta = atan(-normal.y, normal.x) + 3.1415927410125732421875;
    return vec2(theta / 6.283185482025146484375, phi / 3.1415927410125732421875);
}

vec3 lambertDiffuseBRDF(vec3 albedo, float nl)
{
    return albedo * nl;
}

float d_ggx(float nh, float a)
{
    float a2 = a * a;
    float denom = ((nh * nh) * (a2 - 1.0)) + 1.0;
    denom = max(denom * denom, 6.103515625e-05);
    return (a2 * 0.3183098733425140380859375) / denom;
}

float g2_approx(float NdotL, float NdotV, float alpha)
{
    vec2 helper = (vec2(NdotL, NdotV) * 2.0) * (vec2(1.0) / ((vec2(NdotL, NdotV) * (2.0 - alpha)) + vec2(alpha)));
    return max(helper.x * helper.y, 0.0);
}

vec3 f_schlick(vec3 f0, float vh)
{
    return f0 + ((vec3(1.0) - f0) * exp2((((-5.554729938507080078125) * vh) - 6.9831600189208984375) * vh));
}

vec3 specularBRDF(vec3 f0, float roughness, float nl, float nh, float nv, float vh)
{
    float a = roughness * roughness;
    return (f_schlick(f0, vh) * (d_ggx(nh, a) * g2_approx(nl, nv, a))) / vec3(max(4.0 * nv, 9.9999997473787516355514526367188e-06));
}

float attenuate(float dist)
{
    return 1.0 / (dist * dist);
}

float lpToDepth(inout vec3 lp, vec2 lightProj_1)
{
    lp = abs(lp);
    float zcomp = max(lp.x, max(lp.y, lp.z));
    zcomp = lightProj_1.x - (lightProj_1.y / zcomp);
    return (zcomp * 0.5) + 0.5;
}

float PCFCube(samplerCubeShadow shadowMapCube, vec3 lp, inout vec3 ml, float bias, vec2 lightProj_1, vec3 n)
{
    vec3 param = lp;
    float _443 = lpToDepth(param, lightProj_1);
    float compare = _443 - (bias * 1.5);
    ml += ((n * bias) * 20.0);
    vec4 _459 = vec4(ml, compare);
    float result = texture(shadowMapCube, vec4(_459.xyz, _459.w));
    vec4 _471 = vec4(ml + vec3(0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_471.xyz, _471.w));
    vec4 _485 = vec4(ml + vec3(-0.001000000047497451305389404296875, 0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_485.xyz, _485.w));
    vec4 _498 = vec4(ml + vec3(0.001000000047497451305389404296875, -0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_498.xyz, _498.w));
    vec4 _511 = vec4(ml + vec3(0.001000000047497451305389404296875, 0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_511.xyz, _511.w));
    vec4 _524 = vec4(ml + vec3(-0.001000000047497451305389404296875, -0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_524.xyz, _524.w));
    vec4 _537 = vec4(ml + vec3(0.001000000047497451305389404296875, -0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_537.xyz, _537.w));
    vec4 _550 = vec4(ml + vec3(-0.001000000047497451305389404296875, 0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_550.xyz, _550.w));
    vec4 _563 = vec4(ml + vec3(-0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_563.xyz, _563.w));
    return result / 9.0;
}

vec3 sampleLight(vec3 p, vec3 n, vec3 v, float dotNV, vec3 lp, vec3 lightCol, vec3 albedo, float rough, float spec, vec3 f0, int index, float bias, bool receiveShadow)
{
    vec3 ld = lp - p;
    vec3 l = normalize(ld);
    vec3 h = normalize(v + l);
    float dotNH = max(0.0, dot(n, h));
    float dotVH = max(0.0, dot(v, h));
    float dotNL = max(0.0, dot(n, l));
    vec3 direct = lambertDiffuseBRDF(albedo, dotNL) + (specularBRDF(f0, rough, dotNL, dotNH, dotNV, dotVH) * spec);
    direct *= attenuate(distance(p, lp));
    direct *= lightCol;
    if (receiveShadow)
    {
        vec3 param = -l;
        float _624 = PCFCube(shadowMapPoint[0], ld, param, bias, lightProj, n);
        direct *= _624;
    }
    return direct;
}

void main()
{
    vec4 g0 = textureLod(gbuffer0, texCoord, 0.0);
    vec3 n;
    n.z = (1.0 - abs(g0.x)) - abs(g0.y);
    vec2 _654;
    if (n.z >= 0.0)
    {
        _654 = g0.xy;
    }
    else
    {
        _654 = octahedronWrap(g0.xy);
    }
    n = vec3(_654.x, _654.y, n.z);
    n = normalize(n);
    float roughness = g0.z;
    float param;
    uint param_1;
    unpackFloatInt16(g0.w, param, param_1);
    float metallic = param;
    uint matid = param_1;
    vec4 g1 = textureLod(gbuffer1, texCoord, 0.0);
    vec2 occspec = unpackFloat2(g1.w);
    vec3 albedo = surfaceAlbedo(g1.xyz, metallic);
    vec3 f0 = surfaceF0(g1.xyz, metallic);
    float depth = (textureLod(gbufferD, texCoord, 0.0).x * 2.0) - 1.0;
    vec3 p = getPos(eye, eyeLook, normalize(viewRay), depth, cameraProj);
    vec3 v = normalize(eye - p);
    float dotNV = max(dot(n, v), 0.0);
    vec2 envBRDF = texelFetch(senvmapBrdf, ivec2(vec2(dotNV, 1.0 - roughness) * 256.0), 0).xy;
    vec3 envl = shIrradiance(n, shirr);
    vec3 reflectionWorld = reflect(-v, n);
    float lod = getMipFromRoughness(roughness, float(envmapNumMipmaps));
    vec3 prefilteredColor = textureLod(senvmapRadiance, envMapEquirect(reflectionWorld), lod).xyz;
    envl *= albedo;
    envl *= (vec3(1.0) - ((f0 * envBRDF.x) + vec3(envBRDF.y)));
    envl += (prefilteredColor * ((f0 * envBRDF.x) + vec3(envBRDF.y)));
    envl *= (envmapStrength * occspec.x);
    fragColor = vec4(envl.x, envl.y, envl.z, fragColor.w);
    vec3 _818 = fragColor.xyz * textureLod(ssaotex, texCoord, 0.0).x;
    fragColor = vec4(_818.x, _818.y, _818.z, fragColor.w);
    int param_2 = 0;
    float param_3 = pointBias;
    bool param_4 = true;
    vec3 _843 = fragColor.xyz + sampleLight(p, n, v, dotNV, pointPos, pointCol, albedo, roughness, occspec.y, f0, param_2, param_3, param_4);
    fragColor = vec4(_843.x, _843.y, _843.z, fragColor.w);
    fragColor.w = 1.0;
}


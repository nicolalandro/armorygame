#version 330

in vec3 wnormal;
out vec4 fragColor[2];

vec2 octahedronWrap(vec2 v)
{
    return (vec2(1.0) - abs(v.yx)) * vec2((v.x >= 0.0) ? 1.0 : (-1.0), (v.y >= 0.0) ? 1.0 : (-1.0));
}

float packFloatInt16(float f, uint i)
{
    uint bitsInt = i << 12u;
    uint bitsFloat = uint(f * 4095.0);
    return float(bitsInt | bitsFloat);
}

float packFloat2(float f1, float f2)
{
    return floor(f1 * 255.0) + min(f2, 0.9900000095367431640625);
}

void main()
{
    vec3 n = normalize(wnormal);
    vec3 basecol = vec3(0.00032499906956218183040618896484375, 0.80000007152557373046875, 0.0173486061394214630126953125);
    float roughness = 0.5;
    float metallic = 0.0;
    float occlusion = 1.0;
    float specular = 0.5;
    vec3 emissionCol = vec3(0.0);
    float opacity = 0.688875675201416015625;
    if (opacity < 0.99989998340606689453125)
    {
        discard;
    }
    n /= vec3((abs(n.x) + abs(n.y)) + abs(n.z));
    vec2 _112;
    if (n.z >= 0.0)
    {
        _112 = n.xy;
    }
    else
    {
        _112 = octahedronWrap(n.xy);
    }
    n = vec3(_112.x, _112.y, n.z);
    fragColor[0] = vec4(n.xy, roughness, packFloatInt16(metallic, 0u));
    fragColor[1] = vec4(basecol, packFloat2(occlusion, specular));
}


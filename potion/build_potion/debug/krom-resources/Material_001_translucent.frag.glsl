#version 330

uniform samplerCubeShadow shadowMapPoint[1];
uniform vec2 lightProj;
uniform sampler2D senvmapBrdf;
uniform vec4 shirr[7];
uniform int envmapNumMipmaps;
uniform sampler2D senvmapRadiance;
uniform float envmapStrength;
uniform vec3 pointPos;
uniform vec3 pointCol;
uniform float pointBias;
uniform bool receiveShadow;
uniform vec4 casData[20];

in vec3 wnormal;
in vec3 eyeDir;
in vec3 wposition;
out vec4 fragColor[2];

vec3 surfaceAlbedo(vec3 baseColor, float metalness)
{
    return mix(baseColor, vec3(0.0), vec3(metalness));
}

vec3 surfaceF0(vec3 baseColor, float metalness)
{
    return mix(vec3(0.039999999105930328369140625), baseColor, vec3(metalness));
}

vec3 shIrradiance(vec3 nor, vec4 shirr_1[7])
{
    vec3 cl00 = vec3(shirr_1[0].x, shirr_1[0].y, shirr_1[0].z);
    vec3 cl1m1 = vec3(shirr_1[0].w, shirr_1[1].x, shirr_1[1].y);
    vec3 cl10 = vec3(shirr_1[1].z, shirr_1[1].w, shirr_1[2].x);
    vec3 cl11 = vec3(shirr_1[2].y, shirr_1[2].z, shirr_1[2].w);
    vec3 cl2m2 = vec3(shirr_1[3].x, shirr_1[3].y, shirr_1[3].z);
    vec3 cl2m1 = vec3(shirr_1[3].w, shirr_1[4].x, shirr_1[4].y);
    vec3 cl20 = vec3(shirr_1[4].z, shirr_1[4].w, shirr_1[5].x);
    vec3 cl21 = vec3(shirr_1[5].y, shirr_1[5].z, shirr_1[5].w);
    vec3 cl22 = vec3(shirr_1[6].x, shirr_1[6].y, shirr_1[6].z);
    return ((((((((((cl22 * 0.429042994976043701171875) * ((nor.y * nor.y) - ((-nor.z) * (-nor.z)))) + (((cl20 * 0.743125021457672119140625) * nor.x) * nor.x)) + (cl00 * 0.88622701168060302734375)) - (cl20 * 0.2477079927921295166015625)) + (((cl2m2 * 0.85808598995208740234375) * nor.y) * (-nor.z))) + (((cl21 * 0.85808598995208740234375) * nor.y) * nor.x)) + (((cl2m1 * 0.85808598995208740234375) * (-nor.z)) * nor.x)) + ((cl11 * 1.02332794666290283203125) * nor.y)) + ((cl1m1 * 1.02332794666290283203125) * (-nor.z))) + ((cl10 * 1.02332794666290283203125) * nor.x);
}

float getMipFromRoughness(float roughness, float numMipmaps)
{
    return roughness * numMipmaps;
}

vec2 envMapEquirect(vec3 normal)
{
    float phi = acos(normal.z);
    float theta = atan(-normal.y, normal.x) + 3.1415927410125732421875;
    return vec2(theta / 6.283185482025146484375, phi / 3.1415927410125732421875);
}

vec3 lambertDiffuseBRDF(vec3 albedo, float nl)
{
    return albedo * nl;
}

float d_ggx(float nh, float a)
{
    float a2 = a * a;
    float denom = ((nh * nh) * (a2 - 1.0)) + 1.0;
    denom = max(denom * denom, 6.103515625e-05);
    return (a2 * 0.3183098733425140380859375) / denom;
}

float g2_approx(float NdotL, float NdotV, float alpha)
{
    vec2 helper = (vec2(NdotL, NdotV) * 2.0) * (vec2(1.0) / ((vec2(NdotL, NdotV) * (2.0 - alpha)) + vec2(alpha)));
    return max(helper.x * helper.y, 0.0);
}

vec3 f_schlick(vec3 f0, float vh)
{
    return f0 + ((vec3(1.0) - f0) * exp2((((-5.554729938507080078125) * vh) - 6.9831600189208984375) * vh));
}

vec3 specularBRDF(vec3 f0, float roughness, float nl, float nh, float nv, float vh)
{
    float a = roughness * roughness;
    return (f_schlick(f0, vh) * (d_ggx(nh, a) * g2_approx(nl, nv, a))) / vec3(max(4.0 * nv, 9.9999997473787516355514526367188e-06));
}

float attenuate(float dist)
{
    return 1.0 / (dist * dist);
}

float lpToDepth(inout vec3 lp, vec2 lightProj_1)
{
    lp = abs(lp);
    float zcomp = max(lp.x, max(lp.y, lp.z));
    zcomp = lightProj_1.x - (lightProj_1.y / zcomp);
    return (zcomp * 0.5) + 0.5;
}

float PCFCube(samplerCubeShadow shadowMapCube, vec3 lp, inout vec3 ml, float bias, vec2 lightProj_1, vec3 n)
{
    vec3 param = lp;
    float _244 = lpToDepth(param, lightProj_1);
    float compare = _244 - (bias * 1.5);
    ml += ((n * bias) * 20.0);
    vec4 _260 = vec4(ml, compare);
    float result = texture(shadowMapCube, vec4(_260.xyz, _260.w));
    vec4 _272 = vec4(ml + vec3(0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_272.xyz, _272.w));
    vec4 _286 = vec4(ml + vec3(-0.001000000047497451305389404296875, 0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_286.xyz, _286.w));
    vec4 _299 = vec4(ml + vec3(0.001000000047497451305389404296875, -0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_299.xyz, _299.w));
    vec4 _312 = vec4(ml + vec3(0.001000000047497451305389404296875, 0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_312.xyz, _312.w));
    vec4 _325 = vec4(ml + vec3(-0.001000000047497451305389404296875, -0.001000000047497451305389404296875, 0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_325.xyz, _325.w));
    vec4 _338 = vec4(ml + vec3(0.001000000047497451305389404296875, -0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_338.xyz, _338.w));
    vec4 _351 = vec4(ml + vec3(-0.001000000047497451305389404296875, 0.001000000047497451305389404296875, -0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_351.xyz, _351.w));
    vec4 _364 = vec4(ml + vec3(-0.001000000047497451305389404296875), compare);
    result += texture(shadowMapCube, vec4(_364.xyz, _364.w));
    return result / 9.0;
}

vec3 sampleLight(vec3 p, vec3 n, vec3 v, float dotNV, vec3 lp, vec3 lightCol, vec3 albedo, float rough, float spec, vec3 f0, int index, float bias, bool receiveShadow_1)
{
    vec3 ld = lp - p;
    vec3 l = normalize(ld);
    vec3 h = normalize(v + l);
    float dotNH = max(0.0, dot(n, h));
    float dotVH = max(0.0, dot(v, h));
    float dotNL = max(0.0, dot(n, l));
    vec3 direct = lambertDiffuseBRDF(albedo, dotNL) + (specularBRDF(f0, rough, dotNL, dotNH, dotNV, dotVH) * spec);
    direct *= attenuate(distance(p, lp));
    direct *= lightCol;
    if (receiveShadow_1)
    {
        vec3 param = -l;
        float _426 = PCFCube(shadowMapPoint[0], ld, param, bias, lightProj, n);
        direct *= _426;
    }
    return direct;
}

void main()
{
    vec3 n = normalize(wnormal);
    vec3 vVec = normalize(eyeDir);
    float dotNV = max(dot(n, vVec), 0.0);
    vec3 basecol = vec3(0.00032499906956218183040618896484375, 0.80000007152557373046875, 0.0173486061394214630126953125);
    float roughness = 0.5;
    float metallic = 0.0;
    float occlusion = 1.0;
    float specular = 0.5;
    vec3 emissionCol = vec3(0.0);
    float opacity = 0.688875675201416015625;
    if (opacity == 1.0)
    {
        discard;
    }
    vec3 albedo = surfaceAlbedo(basecol, metallic);
    vec3 f0 = surfaceF0(basecol, metallic);
    vec2 envBRDF = texelFetch(senvmapBrdf, ivec2(vec2(dotNV, 1.0 - roughness) * 256.0), 0).xy;
    vec3 indirect = shIrradiance(n, shirr);
    indirect *= albedo;
    vec3 reflectionWorld = reflect(-vVec, n);
    float lod = getMipFromRoughness(roughness, float(envmapNumMipmaps));
    vec3 prefilteredColor = textureLod(senvmapRadiance, envMapEquirect(reflectionWorld), lod).xyz;
    indirect += ((prefilteredColor * ((f0 * envBRDF.x) + vec3(envBRDF.y))) * 1.5);
    indirect *= occlusion;
    indirect *= envmapStrength;
    vec3 direct = vec3(0.0);
    int param = 0;
    float param_1 = pointBias;
    bool param_2 = receiveShadow;
    direct += sampleLight(wposition, n, vVec, dotNV, pointPos, pointCol, albedo, roughness, specular, f0, param, param_1, param_2);
    vec4 premultipliedReflect = vec4(vec3(direct + (indirect * 0.5)) * opacity, opacity);
    float w = clamp((pow(min(1.0, premultipliedReflect.w * 10.0) + 0.00999999977648258209228515625, 3.0) * 100000000.0) * pow(1.0 - (gl_FragCoord.z * 0.89999997615814208984375), 3.0), 0.00999999977648258209228515625, 3000.0);
    fragColor[0] = vec4(premultipliedReflect.xyz * w, premultipliedReflect.w);
    fragColor[1] = vec4(premultipliedReflect.w * w, 0.0, 0.0, 1.0);
}


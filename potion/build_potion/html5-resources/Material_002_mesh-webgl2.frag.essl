#version 300 es
precision mediump float;
precision highp int;

in highp vec3 wnormal;
out highp vec4 fragColor[2];

highp vec2 octahedronWrap(highp vec2 v)
{
    return (vec2(1.0) - abs(v.yx)) * vec2((v.x >= 0.0) ? 1.0 : (-1.0), (v.y >= 0.0) ? 1.0 : (-1.0));
}

highp float packFloatInt16(highp float f, uint i)
{
    uint bitsInt = i << 12u;
    uint bitsFloat = uint(f * 4095.0);
    return float(bitsInt | bitsFloat);
}

highp float packFloat2(highp float f1, highp float f2)
{
    return floor(f1 * 255.0) + min(f2, 0.9900000095367431640625);
}

void main()
{
    highp vec3 n = normalize(wnormal);
    highp vec3 basecol = vec3(0.00032499906956218183040618896484375, 0.80000007152557373046875, 0.0173486061394214630126953125);
    highp float roughness = 0.5;
    highp float metallic = 0.0;
    highp float occlusion = 1.0;
    highp float specular = 0.5;
    highp vec3 emissionCol = vec3(0.0);
    highp float opacity = 0.688875675201416015625;
    if (opacity < 0.99989998340606689453125)
    {
        discard;
    }
    n /= vec3((abs(n.x) + abs(n.y)) + abs(n.z));
    highp vec2 _112;
    if (n.z >= 0.0)
    {
        _112 = n.xy;
    }
    else
    {
        _112 = octahedronWrap(n.xy);
    }
    n = vec3(_112.x, _112.y, n.z);
    fragColor[0] = vec4(n.xy, roughness, packFloatInt16(metallic, 0u));
    fragColor[1] = vec4(basecol, packFloat2(occlusion, specular));
}


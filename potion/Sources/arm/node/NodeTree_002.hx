package arm.node;

@:access(armory.logicnode.LogicNode)@:keep class NodeTree_002 extends armory.logicnode.LogicTree {

	var functionNodes:Map<String, armory.logicnode.FunctionNode>;

	var functionOutputNodes:Map<String, armory.logicnode.FunctionOutputNode>;

	public function new() {
		super();
		this.functionNodes = new Map();
		this.functionOutputNodes = new Map();
		notifyOnAdd(add);
	}

	override public function add() {
		var _TranslateObject = new armory.logicnode.TranslateObjectNode(this);
		_TranslateObject.preallocInputs(4);
		_TranslateObject.preallocOutputs(1);
		var _OnUpdate = new armory.logicnode.OnUpdateNode(this);
		_OnUpdate.property0 = "Update";
		_OnUpdate.preallocInputs(0);
		_OnUpdate.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(_OnUpdate, _TranslateObject, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _TranslateObject, 0, 1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.VectorNode(this, 0.0,0.0,0.003000000026077032), _TranslateObject, 0, 2);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.BooleanNode(this, false), _TranslateObject, 0, 3);
		armory.logicnode.LogicNode.addLink(_TranslateObject, new armory.logicnode.NullNode(this), 0, 0);
		var _SetObjectTransform = new armory.logicnode.SetTransformNode(this);
		_SetObjectTransform.preallocInputs(3);
		_SetObjectTransform.preallocOutputs(1);
		var _OnTimer = new armory.logicnode.OnTimerNode(this);
		_OnTimer.preallocInputs(2);
		_OnTimer.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.FloatNode(this, 4.0), _OnTimer, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.BooleanNode(this, true), _OnTimer, 0, 1);
		armory.logicnode.LogicNode.addLink(_OnTimer, _SetObjectTransform, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.ObjectNode(this, ""), _SetObjectTransform, 0, 1);
		var _Transform = new armory.logicnode.TransformNode(this);
		_Transform.preallocInputs(3);
		_Transform.preallocOutputs(1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.VectorNode(this, 0.40640801191329956,-0.45471999049186707,1.0618200302124023), _Transform, 0, 0);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.RotationNode(this, 0.0,0.0,0.0,1.0), _Transform, 0, 1);
		armory.logicnode.LogicNode.addLink(new armory.logicnode.VectorNode(this, 0.22200000286102295,0.22200000286102295,0.2199999988079071), _Transform, 0, 2);
		armory.logicnode.LogicNode.addLink(_Transform, _SetObjectTransform, 0, 2);
		armory.logicnode.LogicNode.addLink(_SetObjectTransform, new armory.logicnode.NullNode(this), 0, 0);
	}
}